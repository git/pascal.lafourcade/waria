module.exports = function (grunt) {
    grunt.initConfig({
        copy: {
            main: {
                files: [
                    {
                        expand: false,
                        src: ['node_modules/phaser/dist/phaser.min.js'],
                        dest: 'public/js/phaser.min.js',
                        filter: 'isFile'
                    }
                ],
            },
        },

        uglify: {
            my_target: {
                files: {
                    'public/js//level1.min.js': ['src/js/level1/*.js'],
                    'public/js//level2.min.js': ['src/js/level2/*.js'],
                    'public/js//level3.min.js': ['src/js/level3/*.js']
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'public/css//app.min.css': ['src/css/*.css']
                }
            }
        },
        minjson: {
            compile: {
                files: {
                    'public/json/duel0.json': ['src/json/duel0.json'],
                    'public/json/duel1.json': ['src/json/duel1.json'],
                    'public/json/duel2.json': ['src/json/duel2.json'],
                    'public/json/duel3.json': ['src/json/duel3.json'],
                    'public/json/duel4.json': ['src/json/duel4.json'],
                    'public/json/duel5.json': ['src/json/duel5.json'],
                    'public/json/duel6.json': ['src/json/duel6.json'],
                    'public/json/duel7.json': ['src/json/duel7.json'],
                    'public/json/last0.json': ['src/json/last0.json'],
                    'public/json/last1.json': ['src/json/last1.json'],
                    'public/json/last2.json': ['src/json/last2.json'],
                    'public/json/last3.json': ['src/json/last3.json'],
                    'public/json/last4.json': ['src/json/last4.json'],
                    'public/json/last5.json': ['src/json/last5.json'],
                    'public/json/last6.json': ['src/json/last6.json'],
                    'public/json/last7.json': ['src/json/last7.json'],
                    'public/json/team0.json': ['src/json/team0.json'],
                    'public/json/team1.json': ['src/json/team1.json'],
                    'public/json/team2.json': ['src/json/team2.json'],
                    'public/json/team3.json': ['src/json/team3.json'],
                    'public/json/team4.json': ['src/json/team4.json'],
                    'public/json/team5.json': ['src/json/team5.json'],
                    'public/json/team6.json': ['src/json/team6.json'],
                    'public/json/team7.json': ['src/json/team7.json'],
                    'public/json/listBonus.json': ['src/json/listBonus.json']
                }
            }
        }

    })

    grunt.loadNpmTasks('grunt-minjson');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('json', ['minjson']);
    grunt.registerTask('cp', ['copy']);
    grunt.registerTask('ugl', ['uglify', 'cssmin']);
    grunt.registerTask('default', ['cp', 'ugl', 'json']);
}
