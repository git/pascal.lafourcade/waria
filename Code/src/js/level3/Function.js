function selectWord(en, fr) {
    switch (LANG) {
        case 'fr':
            return fr.toString().toUpperCase();
        default:
            return en.toString().toUpperCase();
    }
}

function newGame(father, level, game = new Game(father, level)) {
    if (father.scene.getIndex('Game') !== -1) {
        father.scene.remove('Game');
    }
    father.scene.add('Game', game);
    return game;
}

function chooseTarget(robot, list) {
    let minDist = WIDTH_MAP * 2;
    let l = [];
    list.forEach(function (item) {
        if (item !== robot) {
            let hypot = Math.hypot(item.x - robot.x, item.y - robot.y);
            if (minDist >= hypot) {
                if (minDist > hypot) {
                    minDist = hypot;
                    l = [];
                }
                l.push(item);
            }
        }
    });
    if (l.length > 1) {
        return l[Math.floor(Math.random() * (l.length + 1))];
    } else {
        return l[0];
    }
}

function toJson(name, value, end = false) {
    let val;
    switch (typeof value) {
        case "string":
            val = '"' + name.toString() + '":"' + value + '"';
            break;
        default:
            //console.log(typeof value);
            val = '"' + name.toString() + '":' + value;
            break;
    }
    if (!end)
        val += ','
    return val;
}

function endLineJson(json) {
    if (json.length > 0) {
        if (json[json.length - 1] === ",") {
            json = json.substr(0, json.length - 1);
        }
    }
    return json;
}

function getNextLevel(level = "") {
    let newLevel = level;
    newLevel = newLevel.slice(0, -1) + (parseInt(level[level.length - 1]) + 1).toString();
    console.log(level[level.length - 1]);
    if (newLevel[newLevel.length - 1] >= NUMBER_LEVEL) {
        return level;
    }
    return newLevel;
}

function setCookie(cName, cValue) {
    let d = new Date();
    let day = 360;
    d.setTime(d.getTime() + (day * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cName + "=" + cValue + ";" + expires + ";path=/;SameSite=Lax";
}

function getCookie(cName) {
    let c = document.cookie.split('; ');
    for (let i = 0; i < c.length; i++) {
        let m = c[i].split(('='));
        if (m[0] === cName.toString()) {
            return m[1];
        }
    }
    return "";
}