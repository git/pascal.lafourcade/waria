class Bonus {
    constructor(name, texture, value, type) {
        this.texture = texture;
        this.name = name;
        this.value = value;
        this.type = type;
    }

    useOn(robot) {
        robot.team.addScore(SCORE_BONUS);
    }
}