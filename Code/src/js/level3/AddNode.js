class AddNode extends Phaser.Scene {
    constructor(father, game, selected, ...option) {
        super('AddNode');
        this.father = father;
        this.gameR = game;
        this.selected = selected;
        this.lCategory = [];
        let sizeText = HEIGHT_WINDOW / 14;
        this.style = {font: sizeText.toString() + 'px stencil', fill: "#e2e2e2"};
        this.lastCategory = null;
        if (option === undefined) {
            option = [];
        }
        this.option = option;
    }

    create() {
        let height = HEIGHT_WINDOW / (4 / 3);
        this.add.rectangle(0, 0, WIDTH_WINDOW, HEIGHT_WINDOW, 0x000000).setOrigin(0, 0).setAlpha(0.5);
        this.add.rectangle(0, HEIGHT_WINDOW / 2, WIDTH_WINDOW, height, 0x35363A).setOrigin(0, 0.5);
        this.createButtonCancel();
        if (this.option.length > 0) {
            this.createButtonModify();
        } else {
            this.createButtonAdd();
        }
    }


    addButton(x, y, texture, category = new Category(), value) {
        let btn = this.createButton(x, y, texture);
        btn.on('pointerdown', () => this.click(btn, category, value));
        if (category !== this.lastCategory && this.option.length > 0 && (this.option[0] === value || (this.option[0] !== undefined && this.option[0].length > 0 && this.option[0][0] === value[0] && this.option[0][1] === value [1]))) {
            this.option.splice(0, 1);
            this.lastCategory = category;
            this.click(btn, category, value);
        }
        return btn;
    }

    createButton(x, y, texture, height = HEIGHT_BUTTON_TEXT * 1.3, width = null) {
        let btn = this.add.image(x, y, texture).setOrigin(0.5, 0.5).setInteractive();
        btn.on('pointerover', () => btn.setFrame(1));
        btn.on('pointerout', () => btn.setFrame(0));

        btn.displayHeight = height;
        if (width === null) {
            btn.scaleX = btn.scaleY;
        } else {
            btn.displayWidth = width;
        }
        return btn;
    }

    click(btn, category, value) {
        if (btn.isTinted) {
            category.remove(btn);
        } else {
            category.add(btn, value);
        }
    }

    newCategory(name, single, obligatory) {
        let c = new Category(name, single, obligatory);
        this.lCategory.push(c);
        return c;
    }

    addTitle(x, y, title) {
        let t = this.add.text(x, y, title, this.style).setOrigin(0.5, 0.5);
        if (t.displayWidth > WIDTH_WINDOW) {
            t.displayWidth = WIDTH_WINDOW;
            t.scaleY = t.scaleX;
        }
        return t;
    }

    createButtonCancel() {
        this.createButton(WIDTH_WINDOW / 3, HEIGHT_WINDOW / (16 / 15), 'cancel', HEIGHT_BUTTON_TEXT)
            .on('pointerdown', () => this.cancel());
    }

    createButtonAdd() {
        this.createButton(WIDTH_WINDOW / 1.5, HEIGHT_WINDOW / (16 / 15), 'add', HEIGHT_BUTTON_TEXT)
            .on('pointerdown', () => this.addNode());
    }

    createButtonModify() {
        this.createButton(WIDTH_WINDOW / 1.5, HEIGHT_WINDOW / (16 / 15), 'modify', HEIGHT_BUTTON_TEXT)
            .on('pointerdown', () => this.modifyNode());
    }

    verifyCategory() {
        for (let i = 0; i < this.lCategory.length; i++) {
            if (!this.lCategory[i].validate()) {
                return false;
            }
        }
        return true;
    }

    cancel(newRect) {
        if (newRect !== undefined) {
            newRect.click();
            this.gameR.clickNode();
        }
        this.gameR.applyTree();
        this.scene.resume('Game');
        this.scene.stop('AddNode');
        this.father.scene.remove('AddNode');
    }
}