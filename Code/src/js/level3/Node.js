class Node {
    constructor(type = "") {
        this.type = type;
    }

    getTreeJson(option = "") {
        let json = '{' + toJson("type", this.type);
        json += '"option":[' + option + ']}';
        return json;
    }
}