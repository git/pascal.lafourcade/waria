class HealthBar {
    constructor(scene, name = "", width, height, x, y, valueMax, color, colorMin = COLOR_RED, middle = true) {
        this.bar = new Phaser.GameObjects.Graphics(scene);

        this.width = Math.floor(width);
        this.height = Math.floor(height);
        this.valueMax = valueMax;
        this.value = valueMax;
        this.color = color;
        this.colorMin = colorMin;
        this.bonus = [];

        this.draw();

        scene.add.existing(this.bar);

        height = height - 4;
        let style = {font: height.toString() + 'px stencil', fill: "#000000"};
        this.nameText = scene.add.text(x, y, name, style).setOrigin(0, 0);
        this.nameText.alpha = 0.5;
        if (this.nameText.displayWidth > this.width * 0.9) {
            this.nameText.displayWidth = this.width * 0.9;
            this.nameText.scaleY = this.nameText.scaleX;
        }
        this.setX(x, middle);
        this.setY(y);
    }

    addBonus(bonus) {
        if (bonus.value > 0) {
            this.bonus.push(bonus);
        }
        this.draw();
    }

    removeBonus(bonus) {
        this.bonus.splice(this.bonus.indexOf(bonus), 1);
        this.draw();
    }

    setValue(value) {
        if (value < 0) {
            value = 0;
        }
        if (value > this.valueMax) {
            value = this.valueMax;
        }
        if (value !== this.value) {
            this.value = value;
            this.draw();
        }
    }

    getValue() {
        let value = this.value;
        this.bonus.forEach(bonus => value += bonus.value);
        return value;
    }

    setValueMax(valueMax = this.valueMax) {
        if (valueMax <= 0) {
            return;
        }
        let diff = valueMax - this.valueMax;
        this.valueMax = valueMax;
        this.setValue(this.value + diff);
    }

    decrease(amount) {
        let diff = amount;
        this.bonus.forEach(bonus => {
            diff = bonus.decrease(amount);
            if (diff > 0) {
                this.bonus.splice(this.bonus.indexOf(bonus), 1);
            }
        });
        this.value -= diff;

        diff = 0;
        if (this.value < 0) {
            diff = -this.value;
            this.value = 0;
        }

        this.draw();

        return diff;
    }

    increase(amount) {
        this.value += amount;
        if (this.value > this.valueMax) {
            this.value = this.valueMax;
        }
        this.draw();
    }

    setX(x, middle = true) {
        if (middle)
            x = x - this.width / 2;
        this.bar.setX(x);
        this.nameText.setX(x + 5);
    }

    setY(y) {
        this.bar.setY(y);
        this.nameText.setY(y);
    }

    draw() {
        this.bar.clear();

        //  BG
        this.bar.fillStyle(0x000000);
        this.bar.fillRect(0, 0, this.width, this.height);

        //  Health

        this.bar.fillStyle(0xffffff);
        this.bar.fillRect(2, 2, this.width - 4, this.height - 4);

        let valueMax = this.valueMax;
        this.bonus.forEach(bonus => valueMax += bonus.value);

        let percent = this.value / this.valueMax;

        if (percent < 1 / 3) {
            this.bar.fillStyle(this.colorMin);
        } else {
            this.bar.fillStyle(this.color);
        }

        percent = this.value / valueMax;

        let d = percent * (this.width - 4);

        this.bar.fillRect(2, 2, d, this.height - 4);

        let x = d + 2;

        this.bonus.forEach(bonus => {
            let percent = bonus.value / valueMax;
            this.bar.fillStyle(bonus.color);
            let width = percent * (this.width - 4);
            this.bar.fillRect(x, 2, width, this.height - 4);
            x += width;
        });
    }

    destroy() {
        this.bar.destroy();
        this.nameText.destroy();
    }

    getJson() {
        let json = '{';
        json += toJson("valueMax", this.valueMax) + toJson("color", this.color) + toJson("colorMin", this.colorMin);
        json = endLineJson(json);
        json += '}';
        return json;
    }

    setVisible(boolean) {
        this.bar.setVisible(boolean);
        this.nameText.setVisible(boolean);
    }
}