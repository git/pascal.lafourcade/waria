class Finish extends Phaser.Scene {
    constructor(father, game, score) {
        super('Finish');
        this.father = father;
        this.gameSave = game;
        this.score = score;
        let scoreRecord = parseInt(getCookie(this.gameSave.level));
        if (isNaN(scoreRecord)) {
            scoreRecord = this.score;
        }
        console.log(scoreRecord);
        if (this.score >= scoreRecord) {
            setCookie(this.gameSave.level, this.score);
        }
    }

    create() {
        this.add.rectangle(0, 0, WIDTH_WINDOW, HEIGHT_WINDOW, 0x000000).setOrigin(0, 0).setAlpha(0.5);
        this.add.rectangle(0, HEIGHT_WINDOW / 2, WIDTH_WINDOW, HEIGHT_WINDOW / 2, 0x35363A).setOrigin(0, 0.5);
    }

    addTitle(title) {
        let sizeText = HEIGHT_TITLE / 1.5;
        let style = {font: sizeText.toString() + 'px stencil', fill: "#e2e2e2"};
        let text = this.add.text(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 2, title, style).setOrigin(0.5, 1);
        if (text.displayWidth > WIDTH_WINDOW) {
            text.displayWidth = WIDTH_WINDOW;
            text.scaleY = text.scaleX;
        }
        style = {font: (sizeText / 2).toString() + 'px stencil', fill: "#e2e2e2", align: "center"};
        this.add.text(WIDTH_WINDOW / 2, text.y + text.displayHeight, "SCORE : " + this.score.toString() + "\n" + selectWord("RECORD SCORE : ", "SCORE RECORD : ") + getCookie(this.gameSave.level), style).setOrigin(0.5, 0.5);
    }

    createButton(x, texture) {
        let btn = this.add.image(x, HEIGHT_WINDOW / 8 * 7, texture).setOrigin(0.5, 0.5).setInteractive();
        btn.on('pointerover', () => btn.setFrame(1));
        btn.on('pointerout', () => btn.setFrame(0));

        btn.displayHeight = HEIGHT_BUTTON_TEXT;
        btn.scaleX = btn.scaleY;

        return btn;
    }

    btnRetry(btn) {
        btn.on('pointerdown', () => this.retry());
    }

    retry() {
        this.close();
        console.log("RETRY");
        if (this.father.scene.getIndex('Game') === -1) {
            newGame(this.father);
        }
        this.father.scene.start('Game');
    }

    btnHome(btn) {
        btn.on('pointerdown', () => this.home());
    }

    home() {
        console.log("HOME");
        this.close();
        this.father.scene.remove('Game');
        this.father.scene.start('Type');
    }

    btnNext(btn) {
        btn.on('pointerdown', () => this.next());
    }

    next() {
        console.log("NEXT");
        this.gameSave.setLevel(getNextLevel(this.gameSave.level));
        this.retry();
    }

    close() {
        this.sound.stopAll();
        this.father.scene.remove('GamingBoard');
        this.father.scene.remove('Finish');
    }
}