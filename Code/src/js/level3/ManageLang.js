class ManageLang extends Phaser.Scene {
    constructor(name, father) {
        super(name);
        this.name = name;
        this.father = father;
        this.otherScenes = [];
    }

    addOtherScenes(name) {
        this.otherScenes.push(name);
    }

    create() {
        this.createFlags();
    }

    createFlags() {
        this.diffXFlag = 0;
        this.createFlag('fr');
        this.createFlag('en');
    }

    createFlag(name) {
        let flag = this.add.image(WIDTH_WINDOW - 10 - this.diffXFlag, 10, name).setOrigin(1, 0).setInteractive();
        flag.displayHeight = WIDTH_BUTTON / 2;
        flag.scaleX = flag.scaleY;
        this.diffXFlag = this.diffXFlag + flag.displayWidth + 10;
        flag.on('pointerdown', () => this.setLang(name));
    }

    setLang(lang) {
        if (lang !== LANG) {
            LANG = lang;
            this.father.scene.add('LoadFile', new LoadFile(this.father, this.name));
            this.father.scene.start('LoadFile');
            this.otherScenes.forEach(scene => this.father.scene.stop(scene));
        }
    }
}