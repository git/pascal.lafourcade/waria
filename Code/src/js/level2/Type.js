class Type extends ManageLang {
    constructor(father) {
        super('Type', father);
    }

    preload() {

    }

    create() {
        super.create();
        let style = {font: HEIGHT_TITLE.toString() + 'px stencil', fill: "#e2e2e2"};
        this.add.text(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 6, "WARIA", style).setOrigin(0.5, 0.5);

        let btnDuel = this.add.sprite((WIDTH_WINDOW / 8) * 3, (HEIGHT_WINDOW / 4) * 2, 'duel').setInteractive();
        this.createbutton(btnDuel, 'duel');

        let btnLast = this.add.sprite((WIDTH_WINDOW / 8) * 5, (HEIGHT_WINDOW / 4) * 2, 'last').setInteractive();
        this.createbutton(btnLast, 'last');

        let btnTeam = this.add.sprite(WIDTH_WINDOW / 2, (HEIGHT_WINDOW / 4) * 3, 'team').setInteractive();
        this.createbutton(btnTeam, 'team');

        let btnLoad = this.add.sprite(WIDTH_WINDOW, HEIGHT_WINDOW, 'loadBtn').setInteractive().setOrigin(1, 1);
        btnLoad.displayWidth = btnLoad.displayHeight = WIDTH_BUTTON * 2;
        btnLoad.on('pointerout', () => btnLoad.setFrame(0));
        btnLoad.on('pointerover', () => btnLoad.setFrame(1));
        btnLoad.on('pointerdown', () => this.clickLoadLevel());
    }

    createbutton(btn, level) {
        if (WIDTH_WINDOW > HEIGHT_WINDOW) {
            btn.displayHeight = HEIGHT_WINDOW / 4;
            btn.scaleX = btn.scaleY;
        } else {
            btn.displayWidth = WIDTH_WINDOW / 4;
            btn.scaleY = btn.scaleX;
        }
        btn.on('pointerout', () => btn.setFrame(0));
        btn.on('pointerover', () => btn.setFrame(1));
        btn.on('pointerdown', () => this.clickPlay(level));
    }

    clickPlay(level) {
        if (father.scene.getIndex('LevelSelect') !== -1) {
            this.father.scene.remove('LevelSelect');
        }
        this.father.scene.add('LevelSelect', new LevelSelect(this.father, level))
        this.scene.start('LevelSelect');
    }

    clickLoadLevel() {
        if (father.scene.getIndex('LoadLevel') !== -1) {
            this.father.scene.remove('LoadLevel');
        }
        this.father.scene.add('LoadLevel', new LoadLevel(this.father));
        this.scene.start('LoadLevel');
    }
}