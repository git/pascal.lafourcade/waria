class InitStat {
    constructor(scene, posX, posY, width, height, valueMax = 10) {
        this.scene = scene;
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
        this.category = new CategoryBar(valueMax);
        this.lBar = [];
        this.lText = [];
        this.category.addText(this.scene, selectWord("TOKENS : ", "JETONS : "), this.posX, this.posY, this.height);
    }

    addBar(value, valueMax, color, text = []) {
        this.lText.push(text);
        this.lBar.push(new Bar(this.scene, selectWord(text[0], text[1]), this.width, this.height, this.posX, this.posY, color, this.category, value, valueMax));
        this.posY += this.height * 1.5;
        this.category.setTextPos(this.posX, this.posY);
    }

    getJson() {
        let json = '{';
        json += toJson("valueMax", this.category.valueMax);
        json += '"lBar":[';
        for (let i = 0; i < this.lBar.length; i++) {
            json += this.lBar[i].getJson(this.lText[i]) + ',';
        }
        json = endLineJson(json) + ']}';
        return json;
    }

    destroy() {
        this.category.destroy();
        this.lBar.forEach(bar => bar.destroy());
    }
}