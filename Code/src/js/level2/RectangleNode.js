class RectangleNode {
    constructor(x, y, scene, type, option = []) {
        switch (type.toString().toLowerCase()) {
            case 'attack':
                this.rect = new Phaser.GameObjects.Image(scene, x, y, 'attackNode');
                this.node = new Attack(option[0]);
                this.canAddNode = false;
                this.line = scene.add.graphics();
                break;
            case 'move' :
                this.rect = new Phaser.GameObjects.Image(scene, x, y, 'moveNode');
                this.node = new Move(option[0], option[1], option[2]);
                this.canAddNode = false;
                this.line = scene.add.graphics();
                break;
            case 'condition':
                this.rect = new Phaser.GameObjects.Image(scene, x, y, 'conditionNode');
                this.node = new Condition(option[0], option[1], option[2], option[3], option[4]);
                this.canAddNode = true;
                this.lRect = [];
                this.scene = scene;
                this.line = scene.add.graphics();

                break;
            case 'waria':
                this.rect = new Phaser.GameObjects.Image(scene, x, y, 'logoWaria').setInteractive();
                this.canAddNode = true;
                this.lRect = [];
                this.scene = scene;
                this.scene.add.existing(this.rect);
                this.scene.input.setDraggable(this.rect);
                break;
            default:
                console.log("Création d'un node echoué");
                return;
        }
        if (type.toString().toLowerCase() !== 'waria') {
            this.rect.setFrame(this.node.getFrame());
        }
        this.type = type;
        this.rect.on('pointerdown', () => this.click());
        this.rect.setOrigin(0.5, 0);
        this.rect.displayHeight = HEIGHT_NODE;
        this.rect.scaleX = this.rect.scaleY;
    }

    getSelected() {
        if (this.rect.isTinted) {
            return this;
        }
        if (this.canAddNode) {
            for (let i = 0; i < this.lRect.length; i++) {
                let rep = this.lRect[i].getSelected();
                if (rep != null) {
                    return rep;
                }
            }
        }
        return undefined;
    }

    click() {
        if (this.rect.isTinted) {
            this.deselect();
        } else {
            this.rect.tint = COLOR_SELECTED;
        }
        if (this.canAddNode === true) {
            console.log("THIS CAN ADD NODE");
        }
    }

    deselect() {
        this.rect.clearTint();
    }

    getX() {
        return this.rect.x;
    }

    getY() {
        return this.rect.y;
    }

    addRect(node) {
        if (this.canAddNode === true) {
            this.lRect.push(node);
            this.addLine(node);
            let rect = this.scene.add.existing(node.rect);
            rect.setInteractive();
            this.scene.input.setDraggable(rect);
            if (this.node !== undefined) {
                this.node.addNode(node.node);
            }
        }
    }

    setLine(x, y) {
        this.xOrigin = x;
        this.yOrigin = y;
        this.updateLine(true);
    }

    updateLine(force = false) {
        if ((force === true || this.xLine !== this.rect.x || this.yLine !== this.rect.y) && this.line !== undefined) {
            this.xLine = this.rect.x;
            this.yLine = this.rect.y;
            this.line.clear();
            this.line.lineStyle(WIDTH_LINE, 0xffffff, 1);
            this.line.lineBetween(this.xOrigin, this.yOrigin, this.rect.x, this.rect.y + this.rect.displayHeight / 10);
        }
        if (this.canAddNode === true) {
            this.lRect.forEach(node => this.addLine(node));
        }
    }

    addLine(node) {
        node.setLine(this.rect.x, this.rect.y + this.rect.displayHeight * 0.9);
    }

    getNode() {
        if (this.node === undefined) {
            let lNodes = [];
            this.lRect.forEach(function (element) {
                lNodes.push(element.getNode());
            });
            return lNodes;
        } else if (this.canAddNode) {
            this.node.clearNodes();
            this.lRect.forEach(rect => this.node.addNode(rect.getNode()));
        }
        return this.node;
    }

    sort(sortDesc = (a, b) => a.getX() - b.getX()) {
        if (this.canAddNode) {
            this.lRect.sort(sortDesc);
            this.lRect.forEach(function (element) {
                if (element.canAddNode) {
                    element.sort(sortDesc);
                }
            })
        }
    }

    destroy() {
        this.line.destroy();
        this.rect.destroy();
        if (this.lRect !== undefined) {
            this.lRect.forEach(rect => rect.destroy());
        }
    }

    delete(rect) {
        let id = this.lRect.indexOf(rect);
        if (id === -1) {
            for (let i = 0; i < this.lRect.length; i++) {
                if (this.lRect[i].canAddNode)
                    if (this.lRect[i].delete(rect))
                        return true;
            }
        } else {
            this.lRect.splice(id, 1);
            rect.destroy();
            return true;
        }
        return false;
    }

    getJson(withX = false) {
        let json = '{';
        if (withX) {
            json += toJson("x", this.rect.x) + toJson("y", this.rect.y)
        }
        json += toJson("type", this.type);
        if (this.node !== undefined) {
            json += '"option":[' + this.node.getOptionJson() + '],';
        }
        if (this.canAddNode) {
            json += '"lNode":[';
            this.lRect.forEach(rect => json += rect.getJson(withX) + ',');
            json = endLineJson(json) + ']';
        }
        json = endLineJson(json);
        json += '}';
        return json;
    }

    destroy() {
        this.rect.destroy();
        if (this.line !== undefined) {
            this.line.destroy();
        }
        if (this.lRect !== undefined) {
            this.lRect.forEach(rect => rect.destroy());
        }
    }
}