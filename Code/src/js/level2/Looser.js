class Looser extends Finish {
    constructor(father, game, score) {
        super(father, game, score);
    }

    create() {
        super.create();
        super.addTitle(selectWord("GAME OVER", "PARTIE PERDUE"));
        super.btnRetry(super.createButton(WIDTH_WINDOW / 3, 'retry'));
        super.btnHome(super.createButton(WIDTH_WINDOW / 1.5, 'home'));
    }
}