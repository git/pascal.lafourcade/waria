class Boot extends ManageLang {
    constructor(father) {
        super('Boot', father);
    }

    create() {
        super.create();
        let style = {font: HEIGHT_TITLE.toString() + 'px stencil', fill: "#e2e2e2"};
        this.add.text(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 3, "WARIA", style).setOrigin(0.5, 0.5);
        let btn = this.add.sprite(WIDTH_WINDOW / 2, (HEIGHT_WINDOW / 3) * 2, 'playLetter').setInteractive();
        if (WIDTH_WINDOW > HEIGHT_WINDOW) {
            btn.displayHeight = HEIGHT_WINDOW / 5;
            btn.scaleX = btn.scaleY;
        } else {
            btn.displayWidth = WIDTH_WINDOW / 1.5;
            btn.scaleY = btn.scaleX;
        }
        btn.on('pointerout', () => btn.setFrame(0));
        btn.on('pointerover', () => btn.setFrame(1));
        btn.on('pointerdown', () => this.clickPlay(btn));
    }


    clickPlay(btn) {
        console.log("Play");
        this.father.scene.add('Type', new Type(this.father));
        this.scene.start('Type');
    }
}