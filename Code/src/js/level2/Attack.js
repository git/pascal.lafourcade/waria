class Attack extends Node {
    constructor(percentRange = 1) {
        super("attack");
        if (percentRange > 1 / 3) {
            if (percentRange > 2 / 3) {
                percentRange = 1;
                this.percentBonus = 1;
            } else {
                percentRange = 2 / 3;
                this.percentBonus = 1.5;
            }
        } else {
            percentRange = 1 / 3;
            this.percentBonus = 2;
        }
        this.percentRange = percentRange;
    }

    do(robot) {
        if (robot.haveTarget() && robot.isTargetInRange(this.percentRange)) {
            return robot.attackTarget(this.percentBonus);
        }
        return false;
    }

    getFrame() {
        return this.percentRange * 3 - 1;
    }

    getTreeJson() {
        return super.getTreeJson(this.getOptionJson());
    }


    getOptionJson() {
        return this.percentRange.toString();
    }
}