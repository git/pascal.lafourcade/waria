class Team {
    constructor(name = "enemy", freeForAll = false) {
        this.freeForAll = freeForAll;
        this.listRobot = [];
        this.name = name;
        this.score = 0;
    }

    addEventScore(event, context) {
        this.eventScore = event;
        this.context = context;
    }

    addScore(score) {
        this.score += score;
        if (this.eventScore !== undefined && this.context !== undefined) {
            this.eventScore.call(this.context, this.score);
        }
    }

    addRobot(robot) {
        this.listRobot.push(robot);
        robot.team = this;
    }

    readAll() {
        this.listRobot.forEach(robot => robot.read());
    }

    getRobots() {
        return this.listRobot;
    }

    actualise(listEnemyRobot = []) {
        for (let i = 0; i < this.listRobot.length; i++) {
            if (!this.listRobot[i].isAlive()) {
                this.listRobot.splice(i, 1);
                i -= 1;
            } else {
                if (!this.listRobot[i].haveTarget()) {
                    this.listRobot[i].setTarget(chooseTarget(this.listRobot[i], listEnemyRobot));
                }
            }
        }
        return this.listRobot.length !== 0;

    }

    addEvent(event, context) {
        let value = 0;
        this.listRobot.forEach(robot => {
            robot.addEvent(event, context, value);
            value++;
        });
    }

    modifyValue(damage, speedReload, speed, range) {
        this.setSpeed(speed);
        this.setRange(range);
        this.setSpeedReload(speedReload);
        this.setDamage(damage);
    }

    setRange(range) {
        this.listRobot.forEach(robot => {
            robot.setRange(range);
        });
    }

    setSpeed(speed) {
        this.listRobot.forEach(robot => {
            robot.setSpeed(speed);
        });
    }

    setSpeedReload(speedReload) {
        this.listRobot.forEach(robot => {
            robot.setSpeedReload(speedReload);
        });
    }

    setDamage(damage) {
        this.listRobot.forEach(robot => {
            robot.setDamage(damage);
        });
    }

    drawRange() {
        this.listRobot.forEach(robot => {
            robot.drawRange();
        });
    }

    modifyNodes(lNodes) {
        this.listRobot.forEach(robot => {
            robot.setNodes(lNodes);
        });
    }

    setRobotVisible(boolean) {
        this.listRobot.forEach(robot => robot.setVisible(boolean));
    }
}