class Move extends Node {
    constructor(toward = true, enemyBot = true, typeBonus) { //true = move toward / false = flee from
        super("move");
        this.toward = toward;
        this.enemyBot = enemyBot;
        this.typeBonus = typeBonus;
    }

    do(robot) {
        if (this.enemyBot) {
            if (robot.verifyTarget()) {
                if (this.toward === true) {
                    return this.moveToward(robot);
                } else {
                    return this.fleeFrom(robot);
                }
            }
        } else {

            let bonusTarget = chooseTarget(robot, this.getListBonus());
            if (bonusTarget !== undefined) {
                let ret;
                if (this.toward === true) {
                    ret = this.moveToward(robot, bonusTarget);
                } else {
                    ret = this.fleeFrom(robot, bonusTarget);
                }
                if (!ret) {
                    bonusTarget.useOn(robot);
                    listBonus.splice(listBonus.indexOf(bonusTarget), 1);
                    robot.target = null;
                }
                return true;
            }
        }
        return false;
    }

    getListBonus() {
        if (this.typeBonus !== undefined) {
            let l = [];
            listBonus.forEach(bonusView => {
                if (bonusView.bonus.type === this.typeBonus) {
                    l.push(bonusView);
                }
            });
            return l;
        }
        return listBonus;
    }

    moveToward(robot, target = robot.target) {
        return robot.advanceTo(target);
    }

    fleeFrom(robot, target = robot.target) {
        return robot.fleeFrom(target);
    }

    getFrame() {
        let frame;
        if (this.toward)
            frame = 0;
        else
            frame = 5;
        if (!this.enemyBot)
            switch (this.typeBonus) {
                case "speed":
                    frame += 2;
                    break;
                case "attack":
                    frame += 3;
                    break;
                case "shield":
                    frame += 4;
                    break;
                default:
                    frame += 1;
            }
        return frame;
    }

    getTreeJson() {
        return super.getTreeJson(this.getOptionJson());
    }

    getOptionJson() {
        let json = this.toward + ',' + this.enemyBot + '';
        if (this.typeBonus !== undefined) {
            json += ',"' + this.typeBonus + '"';
        }
        return json;
    }
}