class CategoryBar {
    constructor(valueMax = VALUE_MAX_BAR, startValue = 1) {
        this.startValue = startValue;
        this.valueMax = valueMax;
        this.point = this.valueMax;
    }

    addPoint(point) {
        this.point += point;
        if (this.point < 0) {
            let send = this.point;
            this.point = 0;
            return send;
        }
        if (this.point > this.valueMax) {
            this.point = this.valueMax;
        }
        return 0;
    }

    setValueOf(bar, newValue) {
        let diff = newValue - bar.value;
        let oldValue = bar.value;
        if (this.point >= diff) {
            bar.setValue(newValue);
        } else {
            bar.setValue(bar.value + this.point);
        }
        this.addPoint(oldValue - bar.value);
        this.draw();
    }

    addText(scene, text, posX, posY, height) {
        this.text = text;
        let style = {font: height.toString() + 'px stencil', fill: "#e2e2e2"};
        this.textScene = scene.add.text(posX, posY, this.text, style).setOrigin(0, 0);
        this.draw();
    }

    setTextPos(posX, posY) {
        this.textScene.setX(posX);
        this.textScene.setY(posY);
    }

    setText(text) {
        this.text = text;
        this.draw();
    }

    draw() {
        this.textScene.setText(this.text + this.point);
    }

    destroy() {
        this.textScene.destroy();
    }
}