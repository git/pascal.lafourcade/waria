class PlusNode extends Phaser.Scene {
    constructor(father, game, selected) {
        super('PlusNode');
        this.father = father;
        this.gameR = game;
        this.selected = selected;
    }

    create() {
        this.add.rectangle(0, 0, WIDTH_WINDOW, HEIGHT_WINDOW, 0x000000).setOrigin(0, 0).setAlpha(0.5);
        this.add.rectangle(0, HEIGHT_WINDOW / 2, WIDTH_WINDOW, HEIGHT_WINDOW / 2, 0x35363A).setOrigin(0, 0.5);
        this.createNode();
        this.createButton();
    }

    createNode() {
        let x = WIDTH_WINDOW / 2;
        let y = HEIGHT_WINDOW / 2;
        let diffX = WIDTH_WINDOW / 3;
        let diffY = HEIGHT_WINDOW / 10;
        let sizeText = diffY / 1.5;

        if (ORIENTATION_VERTICAL) {
            sizeText = sizeText / 1.5;
        }

        this.createBtn(x - diffX, y, 'attack', selectWord("ATTACK", "ATTAQUE"), diffX, diffY, () => this.click('attack'));

        this.createBtn(x, y, 'move', selectWord("MOVE", "DEPLACEMENT"), diffX, diffY, () => this.click('move'));

        this.createBtn(x + diffX, y, 'condition', selectWord("CONDITION", "CONDITION"), diffX, diffY, () => this.click('condition'));
    }

    createBtn(x, y, texture, title, diffX, diffY, fn) {
        let btn = this.add.sprite(x, y + diffY, texture).setOrigin(0.5, 0.5).setInteractive()
            .on('pointerdown', fn);
        btn.displayHeight = diffY * 2;
        btn.scaleX = btn.scaleY;
        if (btn.displayWidth > diffX) {
            btn.displayWidth = diffX / 1.1;
            btn.scaleY = btn.scaleX;
        }
        let style = {font: HEIGHT_TITLE.toString() + 'px stencil', fill: "#e2e2e2"};
        let t = this.add.text(x, y - diffY, title, style).setOrigin(0.5, 0.5);
        t.displayWidth = btn.displayWidth;
        t.scaleY = t.scaleX;
        return btn
    }

    click(action) {
        switch (action.toString()) {
            case 'attack':
                this.father.scene.add('AddNode', new AddAttack(this.father, this.gameR, this.selected))
                break;
            case 'move':
                this.father.scene.add('AddNode', new AddMove(this.father, this.gameR, this.selected))
                break;
            case 'condition':
                this.father.scene.add('AddNode', new AddCondition(this.father, this.gameR, this.selected))
                break;
        }
        this.scene.launch('AddNode');
        this.scene.stop('PlusNode');
        this.father.scene.remove('PlusNode');
    }

    createButton() {
        let x = WIDTH_WINDOW / 2;
        let y = HEIGHT_WINDOW - 100;
        let cancelBtn = this.add.image(x, y, 'cancel').setOrigin(0.5, 0.5).setInteractive();
        cancelBtn.on('pointerover', () => cancelBtn.setFrame(1));
        cancelBtn.on('pointerout', () => cancelBtn.setFrame(0));
        cancelBtn.on('pointerdown', () => this.cancel());
        cancelBtn.displayHeight = HEIGHT_WINDOW / 10;
        cancelBtn.scaleX = cancelBtn.scaleY;
    }

    cancel() {
        this.scene.resume('Game');
        this.scene.stop('PlusNode');
        this.father.scene.remove('PlusNode');
    }
}