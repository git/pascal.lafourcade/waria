class LoadFile extends Phaser.Scene {
    constructor(father, nameSon, son) {
        super('LoadFile');
        this.father = father;
        this.son = son;
        this.nameSon = nameSon;

        this.father.scene.pause(this.nameSon);
        this.father.scene.stop(this.nameSon);
        this.father.scene.remove('LoadFile');
    }

    preload() {
        let progressBar = this.add.graphics();
        let progressBox = this.add.graphics();
        let width = WIDTH_WINDOW / 3;
        let height = width / 5;
        let x = WIDTH_WINDOW / 2 - width / 2;
        let y = HEIGHT_WINDOW / 2 - height / 2;

        progressBox.fillStyle(0x222222, 0.8);
        progressBox.fillRect(x, y, width, height);
        this.load.on('progress', function (value) {
            progressBar.clear();
            progressBar.fillStyle(0xffffff, 1);
            progressBar.fillRect(x + 5, y + 5, (width - 10) * value, height - 10);
        });
        this.load.on('complete', () => {
            progressBar.destroy();
            progressBox.destroy();
        });

        this.load.audio('shot', 'assets/sounds/shot.ogg');
        this.load.audio('hit', 'assets/sounds/hit.ogg');

        this.loadImage('en', 'flags/', true);
        this.loadImage('fr', 'flags/', true);

        this.loadImage('bonusSpeed0', 'bonus/', true);
        this.loadImage('bonusSpeed1', 'bonus/', true);
        this.loadImage('bonusSpeed2', 'bonus/', true);
        this.loadImage('bonusAttack0', 'bonus/', true);
        this.loadImage('bonusAttack1', 'bonus/', true);
        this.loadImage('bonusAttack2', 'bonus/', true);
        this.loadImage('bonusShield0', 'bonus/', true);
        this.loadImage('bonusShield1', 'bonus/', true);
        this.loadImage('bonusShield2', 'bonus/', true);

        this.loadImage('enemy', 'robots/');
        this.loadImage('myBot', 'robots/');

        this.loadSprite('playLetter', 550, 150, 'buttons/');
        this.loadSprite('cancel', 550, 150, 'buttons/');
        this.loadSprite('add', 550, 150, 'buttons/');
        this.loadSprite('modify', 550, 150, 'buttons/');
        this.loadSprite('home', 550, 150, 'buttons/');
        this.loadSprite('next', 550, 150, 'buttons/');
        this.loadSprite('retry', 550, 150, 'buttons/');

        this.loadSprite('duel', 269, 262, 'buttons/');
        this.loadSprite('last', 269, 262, 'buttons/');
        this.loadSprite('team', 269, 262, 'buttons/');
        this.loadSprite('flag', 269, 262, 'buttons/');
        this.loadSprite('map', 269, 262, 'buttons/');

        this.loadSprite('play', 100, 100, 'buttons/', true);
        this.loadSprite('bin', 100, 100, 'buttons/', true);
        this.loadSprite('plus', 100, 100, 'buttons/', true);
        this.loadSprite('pencil', 100, 100, 'buttons/', true);
        this.loadSprite('speed', 100, 100, 'buttons/', true);
        this.loadSprite('mute', 100, 100, 'buttons/', true);
        this.loadSprite('save', 100, 100, 'buttons/', true);
        this.loadSprite('bonusBtn', 100, 100, 'buttons/', true);
        this.loadSprite('loadBtn', 100, 100, 'buttons/', true);
        this.loadSprite('back', 200, 200, 'buttons/', true);

        this.loadSprite('shortRange', 300, 150, 'buttons/');
        this.loadSprite('mediumRange', 300, 150, 'buttons/');
        this.loadSprite('longRange', 300, 150, 'buttons/');

        this.loadSprite('shield0', 300, 150, 'buttons/');
        this.loadSprite('shield33', 300, 150, 'buttons/');
        this.loadSprite('shield66', 300, 150, 'buttons/');
        this.loadSprite('shield100', 300, 150, 'buttons/');

        this.loadSprite('bonusShield', 300, 150, 'buttons/');
        this.loadSprite('bonusSpeed', 300, 150, 'buttons/');
        this.loadSprite('bonusAttack', 300, 150, 'buttons/');


        this.loadSprite('myself', 300, 150, 'buttons/');
        this.loadSprite('enemyBot', 300, 150, 'buttons/');
        this.loadSprite('bonus', 300, 150, 'buttons/');

        this.loadSprite('moveToward', 300, 150, 'buttons/');
        this.loadSprite('fleeFrom', 300, 150, 'buttons/');

        this.loadSprite('attackNode', 180, 190, 'nodes/', true);
        this.loadSprite('moveNode', 180, 190, 'nodes/', true);
        this.loadSprite('conditionNode', 180, 190, 'nodes/', true);

        this.loadSprite('rectangle', 400, 400, 'buttons/', true);


        this.loadImage('background', 'others/', true);
        this.loadImage('bullet', 'others/', true);
        this.loadImage('logoWaria', 'others/', true);
        this.loadImage('attack', 'nodes/', true);
        this.loadImage('move', 'nodes/', true);
        this.loadImage('condition', 'nodes/', true);

        if (this.cache.json.exists('listBonus')) {
            this.cache.json.remove('listBonus');
            lSpeedBonus = [];
            lShieldBonus = [];
            lAttackBonus = [];
        }
        this.load.json('listBonus', 'public/json/listBonus.json');
    }

    create() {
        this.loadBonus();
        if (this.father.scene.getIndex(this.nameSon) === -1) {
            this.father.scene.add(this.nameSon, this.son);
        }
        this.father.scene.start(this.nameSon);
    }

    loadBonus() {
        let listBonus = this.cache.json.get('listBonus');
        if (listBonus !== undefined) {
            listBonus.lSpeedBonus.forEach(bonus => lSpeedBonus.push(new BonusSpeed(selectWord(bonus.name.en, bonus.name.fr), bonus.texture, bonus.value, bonus.time)));
            listBonus.lShieldBonus.forEach(bonus => lShieldBonus.push(new BonusShield(selectWord(bonus.name.en, bonus.name.fr), bonus.texture, bonus.value, bonus.time, bonus.color)));
            listBonus.lAttackBonus.forEach(bonus => lAttackBonus.push(new BonusAttack(selectWord(bonus.name.en, bonus.name.fr), bonus.texture, bonus.value)));
        }
    }

    loadSprite(name, width, height, folder = '', force = false) {
        if (this.textures.exists(name) && force) {
            return;
        }
        this.deleteLoad(name);
        if (force) {
            this.load.spritesheet(name, 'assets/' + folder + name + '.png', {
                frameWidth: width,
                frameHeight: height
            });
        } else {
            this.load.spritesheet(name, 'assets/lang/' + LANG + '/' + folder + name + '.png', {
                frameWidth: width,
                frameHeight: height
            });
        }
    }

    loadImage(name, folder = '', force = false) {
        if (this.textures.exists(name) && force) {
            return;
        }
        this.deleteLoad(name);
        if (force) {
            this.load.image(name, 'assets/' + folder + name + '.png');
        } else {
            this.load.image(name, 'assets/lang/' + LANG + '/' + folder + name + '.png');
        }
    }

    deleteLoad(name) {
        if (this.textures.exists(name)) {
            this.textures.remove(name);
        }
    }
}