class AddMove extends AddNode {
    constructor(father, game, selected, add = true) {
        if (!add) {
            super(father, game, selected, selected.node.toward, selected.node.enemyBot, selected.node.typeBonus);
        } else {
            super(father, game, selected);
        }
    }

    create() {
        console.log("MOVE");
        super.create();
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 16, selectWord("ADD NODE : MOVE", "AJOUTER ACTION : DEPLACEMENT"));
        let title = super.addTitle(WIDTH_WINDOW / 4, HEIGHT_WINDOW / (16 / 3), selectWord("FLEE OR MOVE ?", "RECULER OU AVANCER ?"));
        if (title.displayWidth > WIDTH_WINDOW / 2) {
            title.displayWidth = WIDTH_WINDOW / 2;
            title.scaleY = title.scaleX;
        }
        let c1 = super.newCategory('direction', true, true);
        super.addButton(WIDTH_WINDOW / 6, HEIGHT_WINDOW / (16 / 5), 'fleeFrom', c1, false);
        super.addButton(WIDTH_WINDOW / 3, HEIGHT_WINDOW / (16 / 5), 'moveToward', c1, true);
        title = super.addTitle(WIDTH_WINDOW / 4 * 3, HEIGHT_WINDOW / (16 / 3), selectWord("WHO ?", "QUI ?"));
        if (title.displayWidth > WIDTH_WINDOW / 2) {
            title.displayWidth = WIDTH_WINDOW / 2;
            title.scaleY = title.scaleX;
        }
        let c2 = super.newCategory('target', true, true);
        super.addButton(WIDTH_WINDOW / 6 * 4, HEIGHT_WINDOW / (16 / 5), 'enemyBot', c2, true);
        let btn = super.addButton(WIDTH_WINDOW / 6 * 5, HEIGHT_WINDOW / (16 / 5), 'bonus', c2, false);
        title = super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 7), selectWord("WHAT BONUS ? (OPTIONAL)", "QUEL BONUS ? (FACULTATIF)"));
        let c3 = super.newCategory('target', true, false);
        let speed = super.addButton(WIDTH_WINDOW / 3, HEIGHT_WINDOW / (16 / 9), 'bonusSpeed', c3, "speed");
        let attack = super.addButton(WIDTH_WINDOW / 1.5, HEIGHT_WINDOW / (16 / 9), 'bonusAttack', c3, "attack");
        let shield = super.addButton(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 12), 'bonusShield', c3, "shield");
        c2.addEvent(() => this.clickBonus(btn, title, speed, attack, shield));
        this.clickBonus(btn, title, speed, attack, shield);
    }

    clickBonus(btn, ...others) {
        if (btn.isTinted) {
            others.forEach(other => other.setVisible(true));
        } else {
            others.forEach(other => other.setVisible(false));
        }
    }

    addNode() {
        if (super.verifyCategory()) {
            let newRect = new RectangleNode(this.selected.getX(), this.selected.getY() + OFFY_TREE, this.gameR, 'move', [this.lCategory[0].getValue(), this.lCategory[1].getValue(), this.lCategory[2].getValue()]);
            this.selected.addRect(newRect);
            super.cancel(newRect);
        }
    }

    modifyNode() {
        if (super.verifyCategory()) {
            this.selected.node.toward = this.lCategory[0].getValue();
            this.selected.node.enemyBot = this.lCategory[1].getValue();
            this.selected.node.typeBonus = this.lCategory[2].getValue();
            this.selected.rect.setFrame(this.selected.node.getFrame());
            super.cancel();
        }
    }
}