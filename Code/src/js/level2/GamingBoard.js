class GamingBoard extends Phaser.Scene {
    constructor(father) {
        super('GamingBoard');
        this.listTeam = [];
        listBonus = [];
        this.father = father;
        this.end = false;
        this.paused = false;
    }

    preload() {
        this.cameras.main.setViewport(OFFX_MAP, OFFY_MAP, WIDTH_MAP, WIDTH_MAP);
        this.sound.add('shot');
        this.sound.add('hit');
    }

    create() {
        let background = this.add.image(0, 0, 'background').setOrigin(0, 0);
        background.alpha = 0.3;
        background.displayWidth = WIDTH_MAP;
        background.displayHeight = WIDTH_MAP;

        this.time.addEvent({delay: 2000, callback: this.upShield, callbackScope: this});
    }

    update(time, delta) {
        if (!this.end && !this.paused) {
            this.listTeam.forEach(team => {
                if (!team.actualise(this.getEnemyRobotTeam(team))) {
                    this.listTeam.splice(this.listTeam.indexOf(team), 1);
                }
            });
            this.listTeam.forEach(team => team.readAll());


            if (this.getMyTeam() === undefined || this.listTeam.length <= 1) {
                this.end = true;
                this.finish();
            } else {

            }
        }
    }

    addEventMyTeam(event, context) {
        this.getMyTeam().addEvent(event, context);
    }

    getEnemyRobotTeam(team) {
        let list = [];
        this.listTeam.forEach(element => {
            if (team.freeForAll || element !== team) {
                list = list.concat(element.getRobots());
            }
        })
        return list;
    }

    setRobotVisible(boolean = true) {
        this.listTeam.forEach(team => team.setRobotVisible(boolean));
    }

    setEnemyRobotVisible(boolean = true) {
        let myTeam = this.getMyTeam();
        this.listTeam.forEach(team => {
            if (myTeam !== team) {
                team.setRobotVisible(boolean);
            }
        });

    }

    setBonusVisible(boolean = true) {
        listBonus.forEach(bonus => bonus.setVisible(boolean));
    }

    finish() {
        this.pause();
        this.father.finish();
    }

    winner() {
        return this.getMyTeam() !== undefined;
    }

    pause() {
        this.sound.pauseAll();
        this.paused = true;
        this.time.timeScale = 0;
    }

    resume() {
        this.sound.resumeAll();
        this.setRobotVisible(true);
        this.setBonusVisible(true);
        this.paused = false;
        this.time.timeScale = SPEED_GAME;
    }

    addTeam(team) {
        this.listTeam.push(team);
    }


    addBonus(bonus) {
        listBonus.push(bonus);
    }

    modifyNodes(lNodes) {
        let myTeam = this.getMyTeam();
        if (myTeam !== undefined) {
            myTeam.modifyNodes(lNodes);
            return true;
        }
        return false;
    }

    modifyValue(damage, speedReload, speed, range) {
        let myTeam = this.getMyTeam();
        if (myTeam !== undefined) {
            myTeam.modifyValue(damage, speedReload, speed, range);
            return true;
        }
        return false;
    }

    getMyTeam() {
        for (let i = 0; i < this.listTeam.length; i++) {
            if (this.listTeam[i].name === MY_TEAM) {
                return this.listTeam[i];
            }
        }
        return undefined;
    }


    upShield() {
        this.listTeam.forEach(team => team.listRobot.forEach(robot => {
            robot.addShield(SHIELD_PER_SECOND);
        }));
        this.time.addEvent({delay: 1000, callback: this.upShield, callbackScope: this});
    }

}