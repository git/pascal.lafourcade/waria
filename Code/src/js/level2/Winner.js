class Winner extends Finish {
    constructor(father, game, score) {
        super(father, game, score);
        let level = game.level.slice(0, -1);
        let nb = parseInt(getCookie(level));
        let nbNext = parseInt(game.level[game.level.length - 1]) + 2;
        if (isNaN(nb) || nbNext > nb) {
            setCookie(level, nbNext);
        }
    }

    create() {
        super.create();
        super.addTitle(selectWord("YOU WIN", "PARTIE GAGNÉE"));
        super.btnRetry(super.createButton(WIDTH_WINDOW / 4, 'retry'));
        super.btnHome(super.createButton(WIDTH_WINDOW / 2, 'home'));
        if (this.gameSave.level.toLowerCase() !== "unknown" && getNextLevel(this.gameSave.level) !== this.gameSave.level) {
            super.btnNext(super.createButton(WIDTH_WINDOW / 4 * 3, 'next'));
        }
    }
}