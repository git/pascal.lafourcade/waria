class LoadLevel extends ManageLang {
    constructor(father) {
        super('LoadLevel');
        this.father = father;
        this.text = "";
    }

    preload() {
        this.load.html('nameform', 'assets/html/fileJson.html');
    }

    create() {
        super.create();
        new Button(this, 10, 10, WIDTH_BUTTON, WIDTH_BUTTON, 'back', () => this.clickBack()).setOrigin(0, 0);

        let element = this.add.dom(0, HEIGHT_WINDOW / 2).createFromCache('nameform');

        element.setOrigin(0, 0.5);
        element.addListener('click');
        element.addListener('keyup');
        let t = element.getChildByName('text');
        t.placeholder = selectWord("Write a Json file", "Ecrire un fichier Json");
        if (t.cols * 12 < WIDTH_WINDOW * 0.5)
            t.cols = WIDTH_WINDOW * 0.5 / 12;
        element.getChildByName('button').innerHTML = selectWord("launch level", "lancer niveau");
        element.getChildByID('error').style = "font: 20px stencil; color: #FF0000;";
        element.getChildByID('div').style = "width: " + WIDTH_WINDOW + "px; text-align: center;";
        let f = element.getChildByName('file');
        f.addEventListener('change', () => {
            let file = f.files[0];
            let reader = new FileReader();
            reader.readAsText(file)
            reader.onload = () => {
                element.getChildByName('text').value = reader.result;
                this.text = reader.result;
            };
        });

        element.getChildByName('text').value = this.text;

        if (this.errorMessage !== undefined)
            this.setErrorMessage(element);

        element.on('click', (event) => {
            if (event.target.name === 'button') {

                if (this.text !== '') {
                    try {
                        let json = JSON.parse(this.text);
                        element.removeListener('click');

                        element.setVisible(false);

                        let game = newGame(this.father);
                        game.setLevelJson(json);
                        this.scene.start('Game');
                    } catch (e) {
                        this.setErrorMessage(element);
                    }

                }
            }
        });
        element.on('keyup', () => this.text = element.getChildByName('text').value);
    }

    setErrorMessage(element) {
        let error = element.getChildByID('error');
        this.errorMessage = selectWord("this text doesn't correspond to a json file", "ce texte ne correspond pas à un fichier Json");
        error.innerHTML = this.errorMessage
    }

    clickBack() {
        this.scene.start('Type');
    }
}