class BonusShield extends Bonus {
    constructor(name, texture, value, time, color) {
        super(name, texture, value, "shield");
        this.time = time;
        this.color = color;
    }

    useOn(robot, scene) {
        super.useOn(robot);
        let bonus = new BonusHealth(this.value, this.color);
        robot.shield.addBonus(bonus);
        scene.time.addEvent({
            delay: this.time,
            callback: () => robot.shield.removeBonus(bonus)
        });
    }
}