class AddCondition extends AddNode {
    constructor(father, game, selected, add = true) {
        if (!add) {
            if (selected.node.shieldFilter) {
                super(father, game, selected, selected.node.myself, [selected.node.shield, 'shield']);
            } else {
                super(father, game, selected, selected.node.myself, [selected.node.range, 'range']);
            }
        } else {
            super(father, game, selected);
        }

    }

    create() {
        super.create();
        console.log("CONDITION");
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 16, selectWord("ADD NODE : CONDITION", "AJOUTER ACTION : CONDITION"));
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 3), selectWord("WHO ?", "QUI ?"));
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 7), selectWord("TARGET FILTERS", "FILTRES CIBLES"));
        let c1 = super.newCategory('target', true, true);
        super.addButton(WIDTH_WINDOW / 3, HEIGHT_WINDOW / (16 / 5), 'enemyBot', c1, false);
        super.addButton(WIDTH_WINDOW / 1.5, HEIGHT_WINDOW / (16 / 5), 'myself', c1, true);
        let c2 = super.newCategory('shield', true, true);
        super.addButton(WIDTH_WINDOW / 8, HEIGHT_WINDOW / (16 / 9), 'shortRange', c2, [1 / 3, 'range']);
        super.addButton(WIDTH_WINDOW / 4 * 1.5, HEIGHT_WINDOW / (16 / 9), 'mediumRange', c2, [2 / 3, 'range']);
        super.addButton(WIDTH_WINDOW / 4, HEIGHT_WINDOW / (16 / 12), 'longRange', c2, [1, 'range']);
        let line = this.add.graphics();
        line.lineStyle(10, 0xffffff, 1);
        line.lineBetween(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 8), WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 13));
        super.addButton(WIDTH_WINDOW / 8 * 5, HEIGHT_WINDOW / (16 / 9), 'shield0', c2, [0, 'shield']);
        super.addButton(WIDTH_WINDOW / 8 * 7, HEIGHT_WINDOW / (16 / 9), 'shield33', c2, [1 / 3, 'shield']);
        super.addButton(WIDTH_WINDOW / 8 * 5, HEIGHT_WINDOW / (16 / 12), 'shield66', c2, [2 / 3, 'shield']);
        super.addButton(WIDTH_WINDOW / 8 * 7, HEIGHT_WINDOW / (16 / 12), 'shield100', c2, [1, 'shield']);
    }

    addNode() {
        if (super.verifyCategory()) {
            let newRect;
            if (this.lCategory[1].getValue()[1] === 'range') {
                newRect = new RectangleNode(this.selected.getX(), this.selected.getY() + OFFY_TREE, this.gameR, 'condition', [this.lCategory[0].getValue(), false, true, 0, this.lCategory[1].getValue()[0]]);

            } else {
                newRect = new RectangleNode(this.selected.getX(), this.selected.getY() + OFFY_TREE, this.gameR, 'condition', [this.lCategory[0].getValue(), true, false, this.lCategory[1].getValue()[0], 0]);
            }
            this.selected.addRect(newRect);
            super.cancel(newRect);
        }
    }

    modifyNode() {
        if (super.verifyCategory()) {
            this.selected.node.shieldFilter = false;
            this.selected.node.rangeFilter = false;
            if (this.lCategory[1].getValue()[1] === 'range') {
                this.selected.node.rangeFilter = true;
                this.selected.node.range = this.lCategory[1].getValue()[0];
            } else {
                this.selected.node.shieldFilter = true;
                this.selected.node.shield = this.lCategory[1].getValue()[0];
            }
            this.selected.node.myself = this.lCategory[0].getValue();
            this.selected.rect.setFrame(this.selected.node.getFrame());
            super.cancel();
        }
    }
}