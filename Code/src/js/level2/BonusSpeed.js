class BonusSpeed extends Bonus {
    constructor(name = "", texture, value, time) {
        super(name, texture, value, "speed");
        this.time = time;
    }

    useOn(robot, scene) {
        super.useOn(robot);
        robot.increaseSpeedBonus(this.value - 1);
        scene.time.addEvent({delay: this.time, callback: () => robot.decreaseSpeedBonus(this.value - 1)});
    }
}