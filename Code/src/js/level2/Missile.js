class Missile extends Phaser.GameObjects.Image {

    constructor(scene, frame, width, height) {
        super(scene, 0, 0, frame);
        this.visible = false;
        this.setOrigin(0.5, 0.5);
        this.setScale(0.02);
    }
}