class AddAttack extends AddNode {
    constructor(father, game, selected, add = true) {
        if (!add) {
            super(father, game, selected, false, selected.node.percentRange);
        } else {
            super(father, game, selected);
        }

    }

    create() {
        console.log("ATTACK");
        super.create();
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / 16, selectWord("ADD NODE : ATTACK", "AJOUTER ACTION : ATTAQUE"));
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 3), selectWord("WHO ?", "QUI ?"));
        super.addTitle(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 7), selectWord("TARGET FILTERS", "FILTRES CIBLES"));

        super.addButton(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 5), 'enemyBot', super.newCategory('target', true, true), false);
        let c2 = super.newCategory('range', true, true);
        super.addButton(WIDTH_WINDOW / 3, HEIGHT_WINDOW / (16 / 9), 'shortRange', c2, 1 / 3);
        super.addButton(WIDTH_WINDOW / 1.5, HEIGHT_WINDOW / (16 / 9), 'mediumRange', c2, 2 / 3);
        super.addButton(WIDTH_WINDOW / 2, HEIGHT_WINDOW / (16 / 12), 'longRange', c2, 1);
    }

    addNode() {
        if (super.verifyCategory()) {
            let newRect = new RectangleNode(this.selected.getX(), this.selected.getY() + OFFY_TREE, this.gameR, 'attack', [this.lCategory[1].getValue()]);
            this.selected.addRect(newRect);
            super.cancel(newRect);
        }
    }

    modifyNode() {
        if (super.verifyCategory()) {
            this.selected.node.percentRange = this.lCategory[1].getValue();
            this.selected.rect.setFrame(this.selected.node.getFrame());
            super.cancel();
        }
    }
}