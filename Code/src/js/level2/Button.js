class Button extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, width, height, texture, fn, line = 0, visible = true) {
        super(scene, x, y, texture);
        this.name = texture;
        this.setInteractive().setVisible(visible);
        this.displayHeight = height;
        this.displayWidth = width;
        this.line = line;
        this.on('pointerout', () => this.changeFrame(0));
        this.on('pointerover', () => this.changeFrame(1));
        this.on('pointerdown', fn);
        this.changeFrame(0);
        scene.add.existing(this);
    }


    changeFrame(column = 0) {
        this.setFrame(column + this.line * 2);
    }
}