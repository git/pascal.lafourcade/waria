class Description extends ManageLang {
    constructor(father) {
        super('Description', father);
    }

    create() {
        super.create();
        let x = WIDTH_WINDOW / 2;
        let y = HEIGHT_WINDOW;
        let returnBtn = this.add.image(x, y, 'cancel').setOrigin(0.5, 1).setInteractive();
        returnBtn.on('pointerover', () => returnBtn.setFrame(1));
        returnBtn.on('pointerout', () => returnBtn.setFrame(0));
        returnBtn.on('pointerdown', () => this.return());
        returnBtn.displayHeight = HEIGHT_WINDOW / 10;
        returnBtn.scaleX = returnBtn.scaleY;

        this.loadBonus();
    }

    loadBonus() {
        this.width = WIDTH_WINDOW / 3 - 20;
        this.height = HEIGHT_WINDOW / 4;
        let offX;
        let offY;
        let x;
        if (WIDTH_WINDOW > HEIGHT_WINDOW) {
            this.nbWidth = 1;
            this.nbHeight = 3;
            offX = WIDTH_WINDOW / 3;
            offY = 0;
            x = 0;
        } else {
            this.nbWidth = 3;
            this.nbHeight = 1;
            offX = 0;
            offY = HEIGHT_WINDOW / 3.5;
            x = WIDTH_BUTTON / 1.5;
        }
        let y = WIDTH_BUTTON / 1.5;
        this.createList(lShieldBonus, x, y, true);
        x += offX;
        y += offY;
        this.createList(lSpeedBonus, x, y, false);
        x += offX;
        y += offY;
        this.createList(lAttackBonus, x, y, false);
    }

    createList(listBonus, x, y, add = true) {
        this.createCategoryBonus(x, y, this.width * this.nbWidth, this.height * this.nbHeight);
        listBonus.forEach(bonus => {
            let time = "";
            if (bonus.time !== undefined) {
                time = '  -  ' + selectWord("TIME", "TEMPS") + ' : ' + (bonus.time / 1000);
            }
            let value = "";
            if (bonus.value !== undefined) {
                let sign = 'x';
                if (add) {
                    sign = '+';
                }
                value = 'BONUS : ' + sign + bonus.value;
            }
            this.createBonusDescription(x, y, this.width, this.height, bonus.texture, bonus.name, value + time);
            if (this.nbHeight > 1)
                y += this.height;
            if (this.nbWidth > 1)
                x += this.width;
        });
    }

    createCategoryBonus(x, y, width, height) {
        x += 10;
        y += 10;
        this.add.rectangle(x, y, width, height, 0x868686).setOrigin(0, 0);
    }

    createBonusDescription(x, y, width, height, texture, name, text) {
        x += 20;
        y += 20;
        width -= 20;
        height -= 20;
        this.add.rectangle(x, y, width, height, 0xFFFFFF).setOrigin(0, 0);
        let image = this.add.image(x + 5, y + 5, texture).setOrigin(0, 0);
        let style = {fill: "#000000"};
        if (WIDTH_WINDOW > HEIGHT_WINDOW) {
            image.displayHeight = width / 5;
            image.scaleX = image.scaleY;
            style.font = (image.displayHeight / 3).toString() + 'px stencil';
            this.add.text(x + image.displayWidth + 10, y + 5 + image.displayHeight / 2, name.toString(), style).setOrigin(0, 0.5);
            style.font = (image.displayHeight / 4).toString() + 'px stencil';
            this.add.text(x + 5, y + image.displayHeight + (height - image.displayHeight) / 2, text.toString(), style).setOrigin(0, 0.5);
        } else {
            image.displayHeight = height / 2;
            image.scaleX = image.scaleY;
            if (image.displayWidth > width) {
                image.displayWidth = width;
                image.scaleY = image.scaleX;
            }
            style.font = (width / 10).toString() + 'px stencil';
            this.add.text(x + 5, y + image.displayHeight + (height - image.displayHeight) / 3, name.toString(), style).setOrigin(0, 0);
            style.font = (width / 15).toString() + 'px stencil';
            this.add.text(x + 5, y + image.displayHeight + (height - image.displayHeight) / 1.5, text.toString(), style).setOrigin(0, 1);
        }
    }

    return() {
        this.father.scene.start('Game');
        this.father.scene.remove('Description');
    }
}