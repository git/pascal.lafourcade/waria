class BonusView extends Phaser.GameObjects.Image {
    constructor(scene, x, y, bonus) {
        super(scene, x, y, bonus.texture);
        this.setOrigin(0.5, 0.5);
        this.displayHeight = HEIGHT_BONUS;
        this.scaleX = this.scaleY;
        scene.add.existing(this);
        this.bonus = bonus;
    }

    useOn(robot) {
        this.bonus.useOn(robot, this.scene);
        this.destroy();
    }
}