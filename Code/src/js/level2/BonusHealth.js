class BonusHealth {
    constructor(value, color) {
        this.value = value;
        this.color = color;
    }

    decrease(amount) {
        this.value -= amount;
        let diff = 0;
        if (this.value < 0) {
            diff = -this.value;
            this.value = 0;
        }

        return diff;
    }
}