class Category {
    constructor(name = 'undefined', single = true, obligatory = true) {
        this.name = name;
        this.single = single;
        this.lBtn = [];
        this.lValue = [];
        this.obligatory = obligatory;
    }

    getValue() {
        if (this.single) {
            return this.lValue[0];
        }
        return this.lValue;
    }

    validate() {
        return !(this.lBtn.length === 0 && this.obligatory);
    }

    addEvent(fn) {
        this.event = fn;
    }

    add(btn, value) {
        if (this.lBtn.length !== 0 && this.single) {
            this.lBtn[0].clearTint();
            this.lValue = [];
            this.lBtn = [];
        }
        this.lValue.push(value);
        this.lBtn.push(btn);
        btn.tint = COLOR_SELECTED;
        this.oneChange();
    }

    remove(btn) {
        let id = this.lBtn.indexOf(btn);
        if (id !== -1) {
            this.lBtn.splice(id, 1);
            this.lValue.splice(id, 1);
        }
        btn.clearTint();
        this.oneChange();
    }

    oneChange() {
        if (this.event !== undefined) {
            this.event();
        }
    }
}