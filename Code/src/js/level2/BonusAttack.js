class BonusAttack extends Bonus {
    constructor(name, texture, value) {
        super(name, texture, value, "attack");
    }

    useOn(robot) {
        super.useOn(robot);
        robot.setAttackBonus(robot.attackBonus * this.value);
    }
}