class Bar extends HealthBar {
    constructor(scene, name = "", width, height, x, y, color, categoryBar = new CategoryBar(), value = 1, valueMax) {
        if (valueMax !== undefined) {
            super(scene, name, width, height, x, y, valueMax, color, color, false);
        } else {
            super(scene, name, width, height, x, y, categoryBar.valueMax, color, color, false);
        }
        this.name = name;
        this.scene = scene;
        this.scene.input.on('pointerdown', this.doDrag, this);
        let style = {font: height.toString() + 'px stencil', fill: "#e2e2e2"};
        this.text = this.scene.add.text(this.bar.x + this.width + 10, this.bar.y, this.value.toString(), style).setOrigin(0, 0);

        this.categoryBar = categoryBar;
        this.setValue(0);
        this.categoryBar.setValueOf(this, value);
    }

    addEvent(event, context) {
        this.event = event;
        this.context = context;
        this.doEvent();
    }

    doDrag() {
        let pointer = this.scene.input;
        if (pointer.x >= this.bar.x - 10 && pointer.x <= this.bar.x + this.width + 10 && pointer.y >= this.bar.y && pointer.y <= this.bar.y + this.height) {
            this.scene.pauseScene();
            this.clickOn();
        }
    }

    clickOn() {
        this.onMove();
        this.scene.input.on('pointermove', () => this.onMove());
        this.scene.input.on('pointerup', () => this.stopClick());
    }

    stopClick() {
        this.scene.input.off('pointermove');
        this.scene.input.off('pointerup');
    }

    onMove() {
        this.setValWithX(this.scene.input.x);
    }

    doEvent() {
        if (this.event !== undefined && this.context !== undefined) {
            this.event.call(this.context, this.value);
        }
    }

    setValWithX(posX) {
        let oldValue = this.value;
        this.categoryBar.setValueOf(this, Math.round((posX - this.bar.x) / this.width * this.valueMax));
        if (oldValue !== this.value) {
            this.doEvent();
        }
    }

    draw() {
        super.draw();
        let posX = this.value / this.valueMax * this.width - 5;
        this.bar.fillStyle(0x969696);
        this.bar.fillRect(posX, 0, 10, this.height);
        if (this.text !== undefined) {
            this.text.setText(this.value + 1);
        }
    }

    getJson(texts = []) {
        let json = super.getJson();
        json = json.substr(0, json.length - 1) + ',';
        json += '"lText":[';
        texts.forEach(text => {
            json += '"' + text + '",';
        });
        json = endLineJson(json);
        json += ']';
        json += '}';
        return json;
    }

    destroy() {
        super.destroy();
        this.text.destroy();
        this.scene.input.off('pointerdown', this.doDrag, this);
    }
}