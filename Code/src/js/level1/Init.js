let config = {
    type: Phaser.AUTO,
    width: WIDTH_WINDOW,
    height: HEIGHT_WINDOW,
    parent: 'all',
    backgroundColor: '#35363A',
    dom: {
        createContainer: true
    }
};

setCookie("team", 8);

let father = new Phaser.Game(config);
father.scene.add('LoadFile', new LoadFile(father, 'Boot', new Boot(father)));
father.scene.start('LoadFile');
